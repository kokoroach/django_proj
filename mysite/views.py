import datetime
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.template import Context
from django.template.loader import get_template

from .forms import ContactForm
# from django.template import Context
# from django.template.loader import get_template


def hello(request):
    return HttpResponse("<strong>Hello world<strong>")

# def current_datetime(request):
#   now = datetime.datetime.now()
#   html = "<html><body>It is now %s.</body></html>" % now
#   return HttpResponse(html)   

def current_datetime(request):
    now = datetime.datetime.now()
#   template = get_template('current_datetime.html')
#   html = template.render(Context({'current_date': now}))
#   return HttpResponse(html)
    context = {'current_date': now}

    return render(
        request,
        'current_datetime.html',
        context

    )

def hours_ahead(request, offset):

    try:
        offset = int(offset)
    except ValueError:
        raise Http404()

#   dt = datetime.datetime.now() + datetime.timedelta(hours=offset)
#   html = "<html><body>In %s hour(s), it will be %s.</body></html>" % (offset, dt)
#   return HttpResponse(html)

    dt = {
        'next_time': datetime.datetime.now() + datetime.timedelta(hours=offset),
        'hour_offset': offset
    }
    return render(
        request,
        'hours_ahead.html',
        dt
    )

def welcome(request):
    return HttpResponse("Welcome to Math!")

def add(request, num1, num2):
    
    try:
        num1 = int(num1)
        num2 = int(num2)
    except ValueError:
        raise Http404()

    html = "<html><body> The sum of {} and {} is {} </body></html>".format(num1, num2, num1+num2)
    return HttpResponse(html)

def minus(request, num1, num2):
    
    try:
        num1 = int(num1)
        num2 = int(num2)
    except ValueError:
        raise Http404()

    html = "<html><body> The difference of {} and {} is {} </body></html>".format(num1, num2, num1-num2)
    return HttpResponse(html)

def mult(request, num1, num2):
    
    try:
        num1 = int(num1)
        num2 = int(num2)
    except ValueError:
        raise Http404()

    html = "<html><body> The product of {} and {} is {} </body></html>".format(num1, num2, num1*num2)
    return HttpResponse(html)

def sqrt(request, num1):
    
    try:
        num1 = int(num1)
    except ValueError:
        raise Http404()

    html = "<html><body>The square of {} is {}</body></html>".format(num1, num1**num1)
    return HttpResponse(html)


def display_meta(request):
    values = request.META.items()
    values.sort()
    html = []
    for k, v in values:
        html.append('<tr><td>%s</td><td>%s</td></tr>' % (k, v))
    return HttpResponse('<table>%s</table>' % '\n'.join(html))


def contact(request):
    if request.method == 'POST':  # If the form has been submitted...
        form = ContactForm(request.POST)  # A form bound to the POST data
        if form.is_valid():  # All validation rules pass
            # Process the data in form.cleaned_data
            subject = form.cleaned_data.get('subject')
            message = form.cleaned_date.get('message')
            sender = form.cleaned_data('sender')

            print subject
            print message

            return HttpResponseRedirect('/thanks/')
    else:
        form = ContactForm()  # An unbound form

    return render(
        request,
        'contact.html',
        {'form': form}
    )

def thanks(request):
    return HttpResponse('Thank You!')


def login_page(request):

    if 'user' in request.GET and 'passw' in request.GET:
        user = request.GET['user']
        passw = request.GET['passw']

        a = authenticate_function(username=user, password=passw)
        if a:
            books = Book.objects.all()
            return render(
                request,
                'search_results.html',
                {'books': books, 'query': q},
            )
        else:
            return render(request, 'login_form.html', None)
    else:
        return render(request, 'login_form.html', None)
