from django.conf.urls import patterns, include, url
from django.contrib import admin

admin.autodiscover()

from .views import hello, current_datetime, hours_ahead, login_page
from .views import welcome, add, minus, mult, sqrt, display_meta, contact, thanks

urlpatterns = patterns('',
	
    url(r'^hello/$', hello),
    url(r'^time/$', current_datetime),
    url(r'^time/plus/(\d+)/$', hours_ahead),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', welcome),
    #Exercise in Django\

    url(r'^add/(\d+)/(\d+)/$', add),
    url(r'^subtract/(\d+)/(\d+)/$', minus),
    url(r'^(\d+)/(\d+)/$', mult),
    url(r'^(\d+)/$', sqrt),
#    url(r'^meta/$', display_meta,
    url(r'^books/', include('books.urls')),
    url(r'^contacts/$', contact),
    url(r'^thanks/$', thanks),
    url(r'^login/$', login_page),

) 