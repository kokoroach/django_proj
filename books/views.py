from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from authenticate import authenticate_function
from .models import Book
from django.contrib.auth.decorators import login_required
from django.http import Http404

#def search_form(request):
#    return render(request, 'search_form.html')

#@login_required
def search(request):
#    if 'q' in request.GET. and request.GET.get['q']:
#        q = request.GET.get['q']
#        books = Book.objects.filter(t itle_icontains=q)
#        return render(
#            request, 
#            'search_results.html',
#            {'books': books, 'query': q},
#        )
#    error = False
#    ------------------------------------------
#    logout(request)

    a = authenticate_function(username="johncena", password="youcan'tseeme")
    if a:
        login(request, a)
    if not request.session.get('yummy_foods'):
        request.session['yummy_foods'] = []
    request.session['yummy_foods'].append('spaghetti')
    request.session['yummy_foods'].append('arroz')

#    print request.user
#    logout(request)
#    print request.user

    
    errors = []
    if 'q' in request.GET:
        q = request.GET['q']
        if not q:
#           error = True
            errors.append('Please submit a search term.')
        elif len(q) > 20:
            errors.append('Please enter max of 20 characters')    
        else:
            books = Book.objects.filter(title__icontains=q)
            return render(
                request,
                'search_results.html',
                {'books': books, 'query': q},
            )
    return render(request, 'search_form.html', {'errors': errors})


def index(request):
    books = Book.objects.all()
    return render(request, 'books/books.html', {'books': books})

def detail(request, pk):
    try:
        book = Book.objects.get(pk=pk)
    except Book.DoesNotExist:
        raise Http404()

    return render(request, 'books/book.html', {'book': book})