from django.conf.urls import patterns, url

#from .views import search_form, search
from .views import search, index, detail


urlpatterns = patterns( '',
 #   url(r'^search-form/$', views.search_form),
    url(r'^search/$', search),
    url(r'^(\d+)/$', detail, name="detail"),
    url(r'^$', index, name="index")
)