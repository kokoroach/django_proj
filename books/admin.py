

from django.contrib import admin

from .models import Author, Book, Publisher


class AuthorAdmin(admin.ModelAdmin):
    fields = ['email', 'first_name', 'last_name']
    list_display = ['id', 'last_name', 'first_name', 'email']
    search_fields = ['first_name', 'last_name']

class BookAdmin(admin.ModelAdmin):

    fieldsets = [
        ('title', {'fields': ['title']}),
        ('Other information: ', {
            'fields':['authors', 'publisher']
            }),
        ('Date information', {
            'fields': ['publication_date'],
            'classes': ['collapse'],
        }),
    ]

    list_display = ['title', 'publisher', 'was_published_recently','publication_date']
    list_filter  = ['publication_date']
    search_fields = ['title']
    # many to many fields
    filter_horizontal = ['authors']


class BookInline(admin.TabularInline): #From StackInline
    model = Book
    extra = 0 # all existing

class PublisherAdmin(admin.ModelAdmin):
    fields = ['name', 'city', 'country', 'website']
    inlines = [BookInline]
    search_fields = ['name', 'city', 'country', 'website']
    list_display = ['name', 'country', 'city', 'website']




admin.site.register(Author, AuthorAdmin)
admin.site.register(Book, BookAdmin)
admin.site.register(Publisher, PublisherAdmin)